import 'package:flutter/material.dart';
import 'package:module_5/studentcheck.dart';
import 'package:firebase_core/firebase_core.dart';


Future main() async {
WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: FirebaseOptions(
  apiKey: "AIzaSyDR7DSwRsZ7IHeGjmNmJFsVzjVRPmHTYFc",
  authDomain: "student-check-6d21e.firebaseapp.com",
  projectId: "student-check-6d21e",
  storageBucket: "student-check-6d21e.appspot.com",
  messagingSenderId: "596026485829",
  appId: "1:596026485829:web:3a29f1b82d73859f21c72c")

  );

  
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Student Reg',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(title: Text('Student Validation'),
    ),
    body: Center(child: Column(mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[ StudentCheck()],)),
    );
  }
}
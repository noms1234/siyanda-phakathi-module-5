
import 'dart:js';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:firebase_core/firebase_core.dart';

class StudentList extends StatefulWidget {
  const StudentList({super.key});

  @override
  State<StudentList> createState() => _StudentListState();
}

class _StudentListState extends State<StudentList> {

  final Stream<QuerySnapshot> _myList =
  FirebaseFirestore.instance.collection("validation").snapshots();

  
  @override
  Widget build(BuildContext context) {
    TextEditingController _nameFieldCtrlr = TextEditingController();
    TextEditingController _emailFieldCtrlr = TextEditingController();
    TextEditingController _studentnumberFieldCtrlr = TextEditingController();
 
 void _delete(docId) {
  FirebaseFirestore.instance.
  collection("validation")
  .doc(docId)
  .delete()
  .then((value) => print("delete"));
}

void _update(data) {
var collection = FirebaseFirestore.instance.collection("validation");
//print(data);

_nameFieldCtrlr.text = data["Student_Name"];
_studentnumberFieldCtrlr.text = data["Student_Number"];
_emailFieldCtrlr.text = data["email"];



showDialog(
  context: context,
  builder: (_) => AlertDialog(
    title: const Text("Update"),
    content: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        TextField(
          controller: _nameFieldCtrlr,
        ),
        TextField(
          controller:_studentnumberFieldCtrlr,
        ),
        TextField(
          controller:_emailFieldCtrlr,
        ),
        TextButton(onPressed: () {
          collection.doc(data["doc_id"]).update({
            "Student_Name": _nameFieldCtrlr.text,
            "Student_Number": _studentnumberFieldCtrlr,
            "email": _emailFieldCtrlr.text,
            
          });
          Navigator.pop(context);
        }, 
        child: Text("Update"))
      ],
    ),
  )
  );
}

    return StreamBuilder(

      stream: _myList,
      builder: (BuildContext context,
      AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return const Text("Input Invalid");
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        }

        
        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                child: SizedBox(
                  height: (MediaQuery.of(context).size.height),
                  width: (MediaQuery.of(context).size.width),

                  child: ListView(
                    children: snapshot.data!.docs
                    .map((DocumentSnapshot documentSnapshot) {
                      Map<String, dynamic> data =
                         documentSnapshot.data()! as Map<String, dynamic>;
                      return Column(
                        children: [
                          Card(
                            child: Column(
                              children: [
                                ListTile(
                                 title: Text(data['Student_Name']),
                                 subtitle: Text(data['Student_Number']),
                          ),  
                          ButtonTheme(child: ButtonBar(
                            children: [
                              OutlinedButton.icon(onPressed: () {
                                _update(data);                              },  
                              icon: Icon(Icons.edit), label: Text("Edit")
                              ),
                              OutlinedButton.icon(
                                onPressed: () {
                                _delete(data["doc_id"]);
                              },
                                 icon: Icon(Icons.remove),
                                label: Text("Delete"),
                                 )
            
                            ],
                          ))
                        ],
                        ),
                    ),
                        ],
                  );
                        
                    }).toList(),
                  )))
              
            ],
          );
        } else {
          return (Text("No data"));
        }
      }
    );
}
}
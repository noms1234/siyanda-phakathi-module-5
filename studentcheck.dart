

import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:module_5/studentlist.dart';


class StudentCheck extends StatefulWidget {
  const StudentCheck({super.key});

  @override
  State<StudentCheck> createState() => _StudentCheckState();
}

class _StudentCheckState extends State<StudentCheck> {
  @override
  Widget build(BuildContext context) {
   TextEditingController nameControllor = TextEditingController();
  TextEditingController emailControllor = TextEditingController();
  TextEditingController studentnumberController = TextEditingController();
  
  Future _StudentCheck() {
    final name = nameControllor.text;
    final email = emailControllor.text;
    final studentnember = studentnumberController.text;

    final ref = FirebaseFirestore.instance.collection("validation").doc();

    return ref
    .set({"Student_Name": name, "email": email, "Student_Number": studentnember, "doc_id": ref.id})
    .then((value) => {
      nameControllor.text = "",
      studentnumberController.text = "",
      emailControllor.text = "",
    })
    .catchError((onError) => log(onError));
   }  
  

    return Column(
      children: [
        Column(
  children: [
  Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
  child: TextField(
        controller: nameControllor,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular((20)),
            ),
            hintText: "Enter name")),
  ),
   Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
  child: TextField(
        controller: emailControllor,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular((20)),
            ),
            hintText: "Enter Email")
            ,),
  ), 
        Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
  child: TextField(
        controller: studentnumberController,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular((20)),
            ),
            hintText: "Enter Student number"))),
            ElevatedButton(
              onPressed: () {
                _StudentCheck();
              }, child: Text("Add Student validation"))
],
        ),
        StudentList()
      ],
    );
  }
}

